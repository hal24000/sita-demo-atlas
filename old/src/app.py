from visualization.index import app

water_app = app
server = water_app.server

if __name__ == "__main__":
    water_app.run_server(debug=False)
